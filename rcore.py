import json
from collections import namedtuple, deque
from itertools import product
from multiprocessing.sharedctypes import Value
from tqdm import tqdm

words = json.load(open("words.json"))

print(len(words))

Guess = namedtuple("Guess", "word, hits")


def get_guess(word, solution):
    for x, s in zip(word,solution):
        if x == s:
            yield 'v'
        elif x in solution:
            yield 'o'
        else:
            yield 'x'
        
guessmat = {(w,s): ''.join(get_guess(w,s)) for w in tqdm(words) for s in words}

print(guessmat[next(iter(guessmat))])

def is_valid_for_guess(guess, solution):
    return guessmat[guess.word, solution] == guess.hits
        
def all_pos_guesses(word):
    for x in product("vox", repeat=5):
        yield Guess(word, ''.join(x))
        


class Guess_State:
    def __init__(self, parent=None, guess=None):
        if parent is None:
            self.pos = words
            self.guesses = []
        else:
            self.pos = [x for x in parent.pos if is_valid_for_guess(guess, x) and x != guess.word]
            self.guesses = parent.guesses + [guess]
    
    def __len__(self):
        return len(self.pos)
    
    def get_children(self, word):
        for x in all_pos_guesses(word):
            s = Guess_State(self, x)
            if len(s):
                yield s
    
    def get_word_split_value(self, word):
        try:
            return(len(max(self.get_children(word),key=len)))
        except ValueError:
            return 0
    
    def get_best_word(self):
        if not self.guesses:
            return "arise"
        if len(self) >= 6:
            return min(tqdm(words), key=self.get_word_split_value)
        else:
            return min(tqdm(self.pos), key=self.get_word_split_value)
    
    def get_rep(self):
        return ";".join(f"{x.word}-{x.hits}" for x in self.guesses)
    
    def get_last(self):
        if self.guesses:
            return "| "*len(self.guesses) + "+-" + f"{ self.guesses[-1].word}-{ self.guesses[-1].hits}"
        else:
            return "+-"
    
Q = deque()

Q.append(Guess_State())

mx = 0

with open("output.txt", "w") as file:
    while Q:
        v = Q.pop()
        best = v.get_best_word()
        
        if len(v.guesses) > mx:
            mx = len(v.guesses)
        
        file.write(v.get_last()+ '->'+ best+'\n')
        
        for x in v.get_children(best):
            Q.append(x)
            
print("max tries", mx)
            

        
    
    
    


